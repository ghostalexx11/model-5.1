/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul5a2;

/**
 *
 * @author D205
 */
public class User {
    private String ID,nama;
    
    public User(String id, String nama){
        this.ID=id;
        this.nama=nama;
    }
    
    String getId() {
        return ID;
    }
    
    String getNama() {
        return nama;
    }
    
    void setId(String id) {
        this.ID=id;
    }
    
    void setNama(String nama) {
        this.nama=nama;
    }
}
