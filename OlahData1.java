/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modul5a2;

/**
 *
 * @author D205
 */
public class OlahData1 {
    public static void main(String[] args) {
        User u1=new User("M001", "Sofyan");
        System.out.println("Nama user sebelum diganti : "+u1.getNama());
        u1.setNama("Antoniawan");
        System.out.println("Nama user setelah diganti : "+u1.getNama());
        
        u1.setData("M002", "Andy");
        u1.setData("M001", "Andy", "Malang");
        System.out.println("Nama user : "+u1.getNama());
    }
}
